import "./App.css";
import NavBar from "./components/Navbar";
import Home from "./pages/Home";
import About from "./pages/About";
import Contact from "./pages/Contact";
import News from "./pages/News";
import SingleNews from "./pages/SingleNews";
import "bootstrap/dist/css/bootstrap.min.css";
import { Routes, Route } from "react-router-dom";

import navData from "./profile/navbar";

function App() {
  return (
    <div className="App">
      <NavBar navData={navData} />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about-us" element={<About />} />
        <Route path="/contact-us" element={<Contact />} />
        {/* <Route path="/news" element={<News />} />
        <Route path="/news/:title" component={<SingleNews />} /> */}

      <Route path="/news/:title" element={<SingleNews />} />
        <Route path="/news" element={<News />} />
      </Routes>
    </div>
  );
}

export default App;
