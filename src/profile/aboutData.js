const aboutData = [
    {
      title: "Our Story",
      content:
        "Welcome to Aakash Labs! We are a team of passionate individuals dedicated to creating innovative solutions and pushing the boundaries of technology.",
      imageUrl:
        "https://www.jfg-nc.com/wordpress/wp-content/uploads/2016/12/tech-in-everyday-life.jpg",
      altText: "Our Story",
    },
    {
      title: "Our Mission",
      content:
        "Our mission is to make a positive impact on the world through cutting-edge research, development, and collaboration. We believe in the power of technology to transform lives and drive positive change.",
      imageUrl:
        "https://qph.cf2.quoracdn.net/main-qimg-13cf4c62d136a377d132526ba3146c7d",
      altText: "Our Mission",
    },
    {
      title: "Our Philosophy",
      content:
        "Explore our website to learn more about our projects, team members, and the exciting work we're doing. If you have any questions or would like to get involved, feel free to contact us!",
      imageUrl: "https://tromindustries.com/images/about/so4.png",
      altText: "Our Philosophy",
    },
  ];
  
  export default aboutData;