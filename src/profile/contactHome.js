import contact from '../assets/contact.png'

const contactOptions = [
    {
      id: 1,
      imageSrc: contact,
      title: "Talk to a member of our team",
      description: "We'll help you find the right products and pricing for your business.",
      buttonText: "Contact",
      contact: [
        "Address: Laxmi Plaza, Putalisadak",
        "Phone: +977 9843875485",
      ],
    },
  ];

export default contactOptions;