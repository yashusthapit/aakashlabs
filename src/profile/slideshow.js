const slideImages = [
  {
    url: "https://media.istockphoto.com/id/637696304/photo/patan.jpg?s=612x612&w=0&k=20&c=-53aSTGBGoOOqX5aoC3Hs1jhZ527v3Id_xOawHHVPpg=",
    caption: "PAGODA STYLE",
    descp: "The beauty of pagoda style temple with natural beauty.",
  },
  {
    url: "https://images.unsplash.com/photo-1526712318848-5f38e2740d44?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
    caption: "OM MANE PADME HU",
    descp: "Religious OM MANE PADME HUM, that creates a value to Buddhist",
  },
  {
    url: "https://media.istockphoto.com/id/814581732/photo/asian-street-life.jpg?s=2048x2048&w=is&k=20&c=FwZAVO4eKXboGIEl-YhbPrBw6fEmD9MGDm7_lBjipfk=",
    caption: "Busyness",
    descp: "Crowded market and Nepali business",
  }
  
];
export default slideImages;
