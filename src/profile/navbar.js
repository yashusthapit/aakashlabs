const navData = {
  logo: {
    title: "Aakash Labs",
    source: "images/rn.png",
    height: 45,
    width: 60,
  },
  sections: [
    {
      title: "Home",
      href: "/",
    },
    {
      title: "About",
      href: "/about-us",
    },
    {
      title: "Contact Us",
      href: "/contact-us",
    },
    {
      title: "News",
      href: "/news",
    },
  ],
};

export default navData;