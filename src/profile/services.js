import PhoneAndroidRoundedIcon from "@mui/icons-material/PhoneAndroidRounded";
import BugReportRoundedIcon from "@mui/icons-material/BugReportRounded";
import AllInclusiveRoundedIcon from "@mui/icons-material/AllInclusiveRounded";
import TerminalRoundedIcon from "@mui/icons-material/TerminalRounded";
import ComputerRoundedIcon from "@mui/icons-material/ComputerRounded";
import CloudQueueRoundedIcon from "@mui/icons-material/CloudQueueRounded";
const services = [
    {
      icon: <TerminalRoundedIcon fontSize="large" />,
      title: "Software Development",
      description:
        "Crafting tailored software solutions aligned with your vision, driving efficiency and innovation through precise coding.",
    },
    {
      icon: <BugReportRoundedIcon fontSize="large" />,
      title: "Software Testing & QA",
      description:
        "Ensuring impeccable software quality through rigorous testing, assuring reliability and user satisfaction with every release.",
    },
    {
      icon: <AllInclusiveRoundedIcon fontSize="large" />,
      title: "Infrastructure & DevOps",
      description:
        "Building robust foundations and agile workflows for scalability and security while embracing continuous development methodologies.",
    },
    {
      icon: <PhoneAndroidRoundedIcon fontSize="large" />,
      title: "Mobile and Web Development",
      description:
        "Creating seamless digital journeys with user-centric design, marrying aesthetics and functionality for unparalleled cross-platform experiences.",
    },
    {
      icon: <ComputerRoundedIcon fontSize="large" />,
      title: "UX and UI Design",
      description:
        "Envisioning and crafting intuitive user experiences, where artistic UI meets seamless functionality, elevating brand value and user engagement.",
    },
    {
      icon: <CloudQueueRoundedIcon fontSize="large" />,
      title: "Internet of Things",
      description:
        "Forging interconnected smart ecosystems, reshaping industries and daily life through infused intelligence and connectivity.",
    },
  ];

export default services;