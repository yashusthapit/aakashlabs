import conversation from '../assets/conversation.png'
import meeting from '../assets/meeting.png'

const aboutShort = [
    {
      mainTitle: "ABOUT",
      subTitle: "US",
      heading:"Grow Your Business With Us",
      image: meeting,
      buttonText : 'Read More',
      descp: "Aakash Labs combines technology with business intelligence to catalyze change and deliver data driven results. We're a dynamic team of passionate people with technical, creative & digital expertise. Where technology meets marketing.",
    },
  ];
  export default aboutShort;
  