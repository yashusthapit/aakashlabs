import React from "react";
import Card from "react-bootstrap/Card";
import { Button, ListGroup } from "react-bootstrap";
import contactOptions from "../profile/contactHome";

function Contact() {
  return (
    <section
      style={{
        background: "#DDDDDD",
        marginBottom: "20px",
        padding: "20px 0",
      }}
    >
      <div className="container" style={{ fontFamily: "Montserrat,serif" }}>
        <div className="row">
          {contactOptions.map((option) => (
            <div
              className="col-md-6 d-flex align-items-center justify-content-center"
              style={{ background: "#fff", padding: "20px" }}
              key={option.id}
            >
              <div>
                <h3 className="pt-4">CONTACT US</h3>
                <Card style={{ width: "20rem" }}>
                  <Card.Img variant="top" src={option.imageSrc} />
                  <Card.Body>
                    <Card.Title>{option.title}</Card.Title>
                    <Card.Text>{option.description}</Card.Text>
                  </Card.Body>
                  <ListGroup className="list-group-flush">
                    {option.contact.map((contaInfo) => {
                      return <ListGroup.Item>{contaInfo}</ListGroup.Item>;
                    })}
                  </ListGroup>
                  <Card.Body>
                    <Button
                      style={{
                        borderRadius: "1px",
                        backgroundColor: "#212121",
                        border: "none",
                        width: "100px",
                      }}
                    >
                      {option.buttonText}
                    </Button>
                  </Card.Body>
                </Card>
              </div>
            </div>
          ))}
          <div
            className="col-md-6"
            style={{ background: "#fff", padding: "20px" }}
          >
            {/* Image Column */}
            <img
              src="https://img.freepik.com/free-photo/high-angle-desk-arrangement-with-laptop_23-2149013923.jpg" // Replace with the actual image URL
              alt="Contact Location"
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
            />
          </div>
        </div>
      </div>
    </section>
  );
}

export default Contact;
