import React from "react";
import servicesData from '../profile/services'

const Services = () => {
  return (
    <>
      <section
        className="skill-at-glance text-white"
        style={{
          marginTop: "50px",
          background: "#212121",
          padding: "50px 0",
          textAlign: "left",
          fontFamily: "Montserrat,serif",
        }}
      >
        <h3 className="text-center">SERVICES</h3>
        <hr
          style={{
            width: "30px",
            alignContent: "center",
            margin: "10px auto",
            borderTop: "2px solid bold",
          }}
        />
        <div className="container">
          <div className="row">
            {servicesData.map((service, index) => (
              <div className="col-lg-4 col-md-6" key={index}>
                <div className="skill-item">
                  <div className="mb-4">{service.icon}</div>
                  <h3>{service.title}</h3>
                  <p>{service.description}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
    </>
  );
};

export default Services;