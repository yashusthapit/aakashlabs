import React from "react";
import { Container, Image, Button } from "react-bootstrap";
// import FormatQuoteIcon from "@mui/icons-material/FormatQuote";
// import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import aboutShort from "../profile/aboutHome";

const Aboutus = () => {
  return (
    <>
      <section style={{ fontFamily: "Montserrat,serif", textAlign: "left" }}>
        {/* Main Article */}
        <div className="container">
          {/* First Column */}

          {aboutShort &&
            aboutShort.map((about, index) => {
              return (
                <div className="row pt-4 mb-4" key={index}>
                  <div className="col-md-4" key={index}>
                    <h1 style={{ fontSize: "90px", textAlign: "left" }}>
                      {about.mainTitle}
                    </h1>
                    <h1 style={{ fontSize: "90px", textAlign: "left" }}>
                      {about.subTitle}
                    </h1>

                    <h5>{about.heading}</h5>

                    <p>{about.descp} </p>
                    <Button style={{border:"none", borderRadius:"2px"}}>{about.buttonText}</Button>
                  </div>

                  <div className="col-md-8">
                    <img
                      src={about.image}
                      alt="about-image"
                      style={{ height: "100%", width: "100% " }}
                    />
                  </div>
                </div>
              );
            })}

          {/* Second Column */}
        </div>
      </section>
    </>
  );
};

export default Aboutus;
