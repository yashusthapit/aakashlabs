// components/NavBar.js
import React from "react";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { useNavigate } from "react-router-dom";

const NavBar = ({ navData }) => {
  const navigate = useNavigate();

  const handleNavigation = (path) => {
    navigate(path);
  };

  return (
    <Navbar bg="dark" variant="dark" fixed="top" expand="md">
      <Container>
        {navData.logo && (
          <Navbar.Brand href="/">{navData.logo.title}</Navbar.Brand>
        )}
        <Nav className="me-auto" />
        <Nav>
          {navData &&
            navData.sections.map((section, index) => {
              return (
                <Nav.Link
                  key={section.title}
                  onClick={() => handleNavigation(section.href)}
                >
                  {section.title}
                </Nav.Link>
              );
            })}
        </Nav>
      </Container>
    </Navbar>
  );
};

export default NavBar;
