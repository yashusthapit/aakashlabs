import React from "react";
import Carousel from "react-bootstrap/Carousel";
import slideImages from "../profile/slideshow";

function Slideshow() {
  return (
    <Carousel>
      {slideImages &&
        slideImages.map((slide, index) => {
          return (
            <Carousel.Item interval={3000} key={index}>
              <img src={slide.url} alt="Slider image" style={{ width: '100%', height: '100vh' }} />
              <Carousel.Caption style={{marginBottom:"80px", backgroundColor:"#0000008a "}}>
                <h3>{slide.caption}</h3>
                <p>
                  {slide.descp}
                </p>
              </Carousel.Caption>
            </Carousel.Item>
          );
        })}
    </Carousel>
  );
}

export default Slideshow;
