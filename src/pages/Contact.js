import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import "../css/Contact.css";
import { Button } from "react-bootstrap";
import TwitterIcon from "@mui/icons-material/Twitter";
import LinkedInIcon from "@mui/icons-material/LinkedIn";

const ContactSchema = Yup.object().shape({
  name: Yup.string().required("Required"),
  email: Yup.string().email("Invalid email").required("Required"),
  number: Yup.string().required("Required"),
  message: Yup.string().required("Required"),
  agree: Yup.boolean()
    .oneOf([true], "Must agree to terms")
    .required("Required"),
});

const Contact = () => {
  return (
    <section className="contact-area pt-120 pb-0">
      <div className="container">
        <div
          className="contact-wrap"
          style={{
            padding: "80px 80px 60px",
            background: "#FFFFFF",
            boxShadow: "0px 10px 50px rgba(30, 30, 30, 0.1)",
            position: "relative",
            zIndex: "1",
            marginBottom: "-160px",
            borderRadius: "10px",
          }}
        >
          <div className="row">
            {/* First Column */}
            <div
              className="col-xl-8 col-md-8 contact-content pr-80 mb-20"
              style={{ borderRight: "1px solid rgba(4, 0, 23, 0.14)" }}
            >
              <h3 className="contact-title mb-25" style={{ fontSize: "32px" }}>
                Send Me Message
              </h3>
              <Formik
                initialValues={{
                  name: "",
                  email: "",
                  number: "",
                  message: "",
                  agree: false,
                }}
                validationSchema={ContactSchema}
                onSubmit={(values) => {
                  // Handle form submission logic here
                  console.log(values);
                }}
              >
                <Form className="contact-form">
                  <div className="row">
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-6">
                      <div className="contact-form-input mb-30">
                        <Field
                          type="text"
                          name="name"
                          placeholder="Your Name"
                        />
                        <ErrorMessage
                          name="name"
                          component="div"
                          className="error-message"
                        />
                      </div>
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-6">
                      <div className="contact-form-input mb-30">
                        <Field
                          type="email"
                          name="email"
                          placeholder="Email Address"
                        />
                        <ErrorMessage
                          name="email"
                          component="div"
                          className="error-message"
                        />
                      </div>
                    </div>
                    <div className="col-xl-6 col-lg-6 col-md-12 col-sm-6">
                      <div className="contact-form-input mb-30">
                        <Field
                          type="text"
                          name="number"
                          placeholder="Your Number"
                        />
                        <ErrorMessage
                          name="number"
                          component="div"
                          className="error-message"
                        />
                      </div>
                    </div>

                    <div className="col-12">
                      <div className="contact-form-input mb-50 contact-form-textarea">
                        <Field
                          as="textarea"
                          name="message"
                          cols="30"
                          rows="10"
                          placeholder="Feel free to get in touch!"
                        />
                        <ErrorMessage
                          name="message"
                          component="div"
                          className="error-message"
                        />
                      </div>
                    </div>
                    <div className="col-12">
                      <div className="contact-form-input mb-30 contact-form-submit">
                        <div className="contact-form-btn">
                          <Button type="submit">Send Message</Button>
                        </div>
                        <div className="contact-form-condition">
                          <label className="condition_label">
                            I agree that my data is collected and stored.
                            <Field type="checkbox" name="agree" />
                            <span className="check_mark"></span>
                          </label>
                          <ErrorMessage
                            name="agree"
                            component="div"
                            className="error-message"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </Form>
              </Formik>
            </div>
            {/* Second Column */}
            <div className="col-xl-4 col-md-4 contact-info ml-50 mb-20">
              <h3 className="contact-title mb-40">Get In Touch</h3>
              <div className="contact-info-item">
                <h5>Address</h5>
                <p>Laxmi Plaza, Putalisadak</p>
              </div>
              <div className="contact-info-item">
                <h5>Phone</h5>
                <p>+977 9843875485</p>
              </div>
              <div className="contact-info-item">
                <h5>Email</h5>
                <p>info@aakashlabs.com.np</p>
              </div>
              <div className="contact-social">
                <h5>Social Media</h5>
                <TwitterIcon />
                <LinkedInIcon />
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <div className="contact-map">
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14129.58746750865!2d85.3226488!3d27.7050303!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19a82386e115%3A0xd09d60ef6582dc35!2sInformatics%20Consultancy!5e0!3m2!1sen!2snp!4v1701330465781!5m2!1sen!2snp"
          width="100%"
          height="300"
          style={{ border: "0" }}
          allowFullScreen=""
          loading="lazy"
          referrerPolicy="no-referrer-when-downgrade"
        ></iframe>
      </div> */}
    </section>
  );
};

export default Contact;
