import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";
import Card from "react-bootstrap/Card";
import Breadcrumb from "react-bootstrap/Breadcrumb";

const SingleNews = () => {
  const { title } = useParams();
  const [article, setArticle] = useState(null);

  useEffect(() => {

    //This is the API that is given to me. While fetching the data of particular news it was creating error so I used the next news API.
    // const apiKey = 'd477a76b7585b8e31b4d6b322e995c74';
    // const url = `https://gnews.io/api/v4/search?q=${encodeURIComponent(title)}&lang=en&country=us&max=10&apikey=${apiKey}`;

    const apiKey = "24a2e1b201734c02bf91b5810c3c66d2";
    const url = `https://newsapi.org/v2/top-headlines?country=us&apiKey=${apiKey}&q=${encodeURIComponent(
      title
    )}`;

    axios
      .get(url)
      .then((response) => {
        if (response.data.articles && response.data.articles.length > 0) {
          setArticle(response.data.articles[0]);
        } else {
          console.error("Article not found");
        }
      })
      .catch((error) => {
        console.error("Error fetching article:", error);
      });
  }, [title]);

  const renderBanner = () => {
    if (article && article.urlToImage) {
      return (
        <Card.Img
          variant="top"
          src={article.urlToImage}
          style={{
            width: "100%",
            height: "30rem",
            objectFit: "cover",
            marginBottom: "20px",
          }}
        />
      );
    }
    return null;
  };

  return (
    <div className="single-news container">
      <Breadcrumb style={{ marginTop: "60px" }}>
        <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
        <Breadcrumb.Item active>{article?.title}</Breadcrumb.Item>
      </Breadcrumb>

      {article ? (
        <Card style={{marginBottom:"60px"}}>
          {renderBanner()}
          <Card.Body>
            <Card.Title>{article.title}</Card.Title>
            <Card.Text>{article.description}</Card.Text>
            <Card.Text>{article.content}</Card.Text>
          </Card.Body>
        </Card>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
};

export default SingleNews;
