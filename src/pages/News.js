import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

const News = () => {
  const [news, setNews] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const cardsPerPage = 6;

  useEffect(() => {
    const apiKey = "24a2e1b201734c02bf91b5810c3c66d2";
    const url = `https://newsapi.org/v2/top-headlines?country=us&apiKey=${apiKey}`;

    console.log("Fetching URL:", url);
    axios
      .get(url)
      .then((response) => {
        setNews(response.data.articles);
        console.log(response.data.articles);
      })
      .catch((error) => {
        console.error("Error fetching news:", error);
      });
  }, []);

  const renderImage = (urlToImage) => {
    if (urlToImage) {
      return (
        <Card.Img variant="top" src={urlToImage} style={{ height: "200px", objectFit: "cover" }} />
      );
    } else {
      return (
        <Card.Img
          variant="top"
          src="https://via.placeholder.com/200x150.png?text=No+Image"
          style={{ height: "200px", objectFit: "cover" }}
        />
      );
    }
  };

  const indexOfLastCard = currentPage * cardsPerPage;
  const indexOfFirstCard = indexOfLastCard - cardsPerPage;
  const currentCards = news.slice(indexOfFirstCard, indexOfLastCard);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <div className="news-container container" style={{ marginTop: "60px" }}>
      <h2 className="m-4">Latest News</h2>
      <div className="row">
        {currentCards.map((item, index) => (
          <div className="col-md-4 mb-4" key={index}>
            <Card style={{ height: "100%" }}>
              {renderImage(item.urlToImage)}
              <Card.Body>
                <Card.Title>{item.title}</Card.Title>
                <Card.Text>{item.description}</Card.Text>
                <Button variant="primary">
                  <Link to={`/news/${encodeURIComponent(item.title)}`} style={{textDecoration:"none",color:"white"}}>Read More</Link>
                </Button>
              </Card.Body>
            </Card>
          </div>
        ))}
      </div>
      <div className="pagination">
        {Array.from({ length: Math.ceil(news.length / cardsPerPage) }, (_, i) => i + 1).map(
          (pageNumber) => (
            <Button
              style={{marginLeft:"3px", marginBottom:"20px"}}
              key={pageNumber}
              onClick={() => paginate(pageNumber)}
              variant={currentPage === pageNumber ? "primary" : "outline-primary"}
            >
              {pageNumber}
            </Button>
          )
        )}
      </div>
    </div>
  );
};

export default News;
