// About.js
import React from "react";
import aboutData from "../profile/aboutData";

const About = () => {
  return (
    <div
      className="container-fluid text-black"
      style={{
        margin: "0 auto",
        paddingTop: "20px",
        marginTop: "50px",
        textAlign: "left",
        fontFamily: "Montserrat,serif",
        background: "#DDDDDD",
      }}
    >
      <h2 className="text-center mb-5">About Us</h2>
      {aboutData.map((section, index) => (
        <div key={index} className="row">
          {index % 2 === 0 ? (
            <>
              <div className="col-md-6" style={{ textAlign: "right", padding:'40px' }}>
                <h3>{section.title}</h3>
                <p style={{ fontSize: "20px" }}>{section.content}</p>
              </div>
              <div className="col-md-6">
                <img
                  src={section.imageUrl}
                  alt={section.altText}
                  style={{ height: "60%", width: "auto", borderRadius: "10px" }}
                />
              </div>
            </>
          ) : (
            <>
              <div className="col-md-6" style={{ textAlign: "right" }}>
                <img
                  src={section.imageUrl}
                  alt={section.altText}
                  style={{ height: "60%", width: "auto", borderRadius: "10px" }}
                />
              </div>
              <div className="col-md-6" style={{ textAlign: "left" }}>
                <h3>{section.title}</h3>
                <p style={{ fontSize: "20px",  }}>{section.content}</p>
              </div>
            </>
          )}
        </div>
      ))}
    </div>
  );
};

export default About;
