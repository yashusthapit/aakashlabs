import React from "react";
import Slideshow from "../components/Slider";
import Aboutus from "../components/About";
import Services from "../components/Services";
import Contact from "../components/Contact";

export default function Home() {
  return (
    <>
      <Slideshow />
      <Aboutus />
      <Services />
      <Contact />
    </>
  );
}
